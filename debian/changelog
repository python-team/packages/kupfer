kupfer (327+git20240901-1) unstable; urgency=medium

  * QA upload.
  * New upstream snapshot.
    + Include fix for command execution. (Closes: #1082576)

 -- Boyuan Yang <byang@debian.org>  Sat, 26 Oct 2024 18:08:45 -0400

kupfer (327-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Sun, 12 May 2024 09:20:11 -0400

kupfer (326-2) unstable; urgency=medium

  * QA upload.
  * Orphan the package.

 -- Boyuan Yang <byang@debian.org>  Sun, 03 Mar 2024 11:30:35 -0500

kupfer (326-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Mon, 26 Feb 2024 19:20:05 -0500

kupfer (325-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Sun, 21 Jan 2024 10:30:37 -0500

kupfer (324-1) unstable; urgency=medium

  * New upstream release.
  * debian/changelog: Use upstream versioning scheme, use 324
    instead of 0+v324.

 -- Boyuan Yang <byang@debian.org>  Tue, 09 Jan 2024 08:55:15 -0500

kupfer (0+v323-1) unstable; urgency=medium

  * New upstream release. (Closes: #994742)
  * debian/control:
    + Bump Standards-Version to 4.6.2.
    + Bump debhelper compat to v13.
    + No longer use Build-Depends-Indep field, not useful.
    + Add new build-dependency python3-pygments for missing
      doc build feature.
    + Add myself into uploaders list.
    + Drop Hugo Lefeuvre from uploaders due to retirement.
      (Closes: #1050281)
  * debian/copyright: Slight updates.
  * debian/patches/0002-ayanata-appindicator.patch: Refresh patch
    on using Ayanata AppIndicator.
  * debian/patches/0003-Fix-waf-shebang.patch: Fix waf shebang.

 -- Boyuan Yang <byang@debian.org>  Wed, 06 Dec 2023 14:25:23 -0500

kupfer (0+v320-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + kupfer: Drop versioned constraint on gir1.2-wnck-3.0 in Recommends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 28 May 2022 01:17:20 +0100

kupfer (0+v320-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 21 Aug 2021 13:38:17 +0100

kupfer (0+v320-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Add missing build dependency on dh addon.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Hugo Lefeuvre ]
  * New upstream release.
    - Use itstool instead of xml2po.
  * debian/watch: Point to new upstream repo, use version=4.
  * debian/control: Build-Depend on itstool and not deprecated
    gnome-doc-utils (Closes: #947533).
  * debian/rules: use --no-hooks

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 25 Oct 2020 14:50:07 +0000

kupfer (0+v319-5) unstable; urgency=medium

  * Fix ayatana patch: misplaced else block leads to AppIndicator3 being
    systematically imported when AyatanaAppIndicator3 is available.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 08 Aug 2019 10:40:15 +0200

kupfer (0+v319-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Hugo Lefeuvre ]
  * Switch to Ayatana AppIndicator (Closes: #906043):
    - Patch by Mike Gabriel, thanks!
    - debian/patches: Add ayatana-appindicator.patch.
    - debian/control: Prefer building against Ayatana AppIndicator shared lib.
    - Additionally provide 16x16 application icon, fixes display problem when
      kupfer is used in AppIndicator mode on i3 (which lacks AppIndicator and
      triggers the fallback GtkStatusIcon code in AppIndicator).
  * debian/control:
    - Bump Standards-Version to 4.4.0.
    - Depend on debhelper dependency to 12.
    - Remove non existing binary dependency python3-wnck.
    - Instead recommend gir1.2-wnck-3.0 (Closes: #876754).
  * debian/copyright:
    - Update copyright years.
  * Run wrap-and-sort -a.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 04 Aug 2019 13:40:25 +0200

kupfer (0+v319-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python3-Version field

  [ Hugo Lefeuvre ]
  * debian/control:
    - Bump Standards-Version to 4.1.5.
    - Depend on debhelper >= 11 instead of >= 10.
    - Update Vcs-* fields for Salsa migration.
  * Bump compat from 10 to 11.
  * debian/copyright:
    - Update copyright years.
    - Use HTTPS protocol in d/copyright urls.

 -- Hugo Lefeuvre <hle@debian.org>  Tue, 31 Jul 2018 20:22:19 +0800

kupfer (0+v319-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control:
    - Bump Standards-Version to 4.1.0.
    - Add missing dh-python Build-Dependency.
  * debian/rules:
    - Build and install documentation in dh_installdocs instead of
      dh_auto_install.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 24 Sep 2017 18:26:06 +0200

kupfer (0+v319-1) experimental; urgency=low

  * New upstream release (Closes: #854297).
    - Port to Python 3.
    - Port to GTK+3 (Closes: #827350)
    - Support MATE (Closes: #778313).
    - Fix support of Firefox bookmarks (Closes: #787094).
  * Bump compatibility level to 10.
  * debian/control, debian/copyright:
    - Update Hugo Lefeuvre's e-mail address.
  * debian/control:
    - Bump Standards-Version to 3.9.8.
    - Bump debhelper Build-Dependency to (>= 10).
    - Update all Build-Dependencies and Dependencies to match new upstream
      version (Closes: #844153).
    - Update Homepage field.
    - Use HTTPS protocol in Vcs-Browser field.
    - Remove useless X-Python-Version field and add X-Python3-Version.
  * debian/copyright:
    - Remove copyright fields for files that are no longer present.
    - Update Source field to match new website.
    - Update copyright years.
    - Use HTTPS protocol in Format field.
  * debian/rules:
    - don't pass --no-install-nautilus-extension to waf script anymore since
      kupfer has been ported to GTK+3 and pyGI.
  * Remove debian/menu (useless because of provided .desktop files).
  * debian/patches:
    - Drop all patches, either integrated by upstream or deprecated.
    - Add debian/patches/fix-desktop-file.patch.
  * Remove debian/kupfer.py, debian/install and stop overriding dh_fixperm in
    debian/rules: Upstream now provides entry points.

 -- Hugo Lefeuvre <hle@debian.org>  Tue, 04 Apr 2017 17:41:23 +0200

kupfer (0+v208-6) unstable; urgency=medium

  * Team upload.
  * Update debian/patches/keyring-3.patch to work with modern versions of
    Keyring (use public API where possible instead of relying on internals).

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 25 Feb 2016 21:24:45 +0300

kupfer (0+v208-5) unstable; urgency=medium

  * debian/patches/bookmarks-places.sqlite.patch:
    - Fix the error when reading firefox bookmark files (Closes: #749067).

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sat, 07 Jun 2014 16:16:36 +0200

kupfer (0+v208-4) unstable; urgency=low

  * debian/control:
    - New Maintainer and co-maintainer (Closes: #741022),
        PAPT as maintainer,
        Hugo Lefeuvre as uploader.
    - Changed kupfer's source webpage: The link was dead.
  * debian/copyright:
    - Changed kupfer's source webpage: The link was dead.
    - New copyright line for the new maintainer.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Wed, 21 May 2014 15:57:00 +0200

kupfer (0+v208-3) unstable; urgency=medium

  * Acknowledge NMU, thanks Sebastian!
  * debian/patches/dbus.patch:
    - Fix crash by initializing D-Bus (Closes: #717105) (LP: #1038434).
  * debian/patches/help.patch:
    - Workaround missing xml file, which prevents documentation from
      being loaded (Closes: #679296).
  * debian/patches/locales.patch:
    - Fix crash with locales not supporting dot as decimal point
      separator (Closes: #680008).
  * debian/patches/tracker-needle.patch:
    - Spawn tracker-needle instead of tracker-search-tool (Closes: #680003).
  * debian/control:
    - Use canonical URIs for Vcs-* fields.
    - Bump Standards-Version to 3.9.5.
  * debian/watch:
    - Adjust watch file to point to GitHub.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 19 Dec 2013 21:23:22 +0100

kupfer (0+v208-2.1) unstable; urgency=low

  * Non-maintainer upload with maintainer approval.
  * debian/patches/keyring-3.patch: python-keyring dropped support for
    deprecated keyring.backend. Make kupfer compatible with the new backend
    location. (Closes: #728468)

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 05 Dec 2013 21:04:54 +0100

kupfer (0+v208-2) unstable; urgency=low

  * debian/patches/uses_fragment.patch:
    - Do not die if urlparse doesn't have uses_fragment method anymore,
      recent Python versions dropped it (Closes: #678580).

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 24 Jun 2012 12:34:02 +0200

kupfer (0+v208-1) unstable; urgency=low

  * New upstream release.

 -- Luca Falavigna <dktrkranz@debian.org>  Mon, 11 Jun 2012 22:51:14 +0200

kupfer (0+v207-2) unstable; urgency=low

  * debian/compat:
    - Bump compatibility level to 9.
  * debian/rules:
    - Remove autogenerated files during clean phase (Closes: #671316).
  * debian/source.lintian-overrides:
    - No longer needed, lintian bug has been fixed.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 27 May 2012 11:46:08 +0200

kupfer (0+v207-1) unstable; urgency=low

  * New upstream release.
    - waf script is now provided unpacked.
  * debian/control:
    - Bump Standards-Version to 3.9.3.
  * debian/copyright:
    - Update new copyright information.
  * debian/source.lintian-overrides:
    - Override source-contains-waf-binary tag, waf script available in
      the tarball is actually the waf-light one.
  * debian/rules:
    - get-orig-source target is no longer needed.
  * debian/watch:
    - Adjust watch file to fetch xz tarballs.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 01 Mar 2012 23:17:02 +0100

kupfer (0+v206+dfsg-1) unstable; urgency=low

  * Upstream tarball repacked to provide unpacked waf (Closes: #654480).
  * debian/patches/rhythmbox.patch:
    - Dropped, no longer useful: rhythmbox-client has been restored.
  * debian/control:
    - Suggest python-qrencode, required by QR module (Closes: #653541).
  * debian/rules:
    - Remove .pyc files on clean.
    - Implement a get-orig-source target to repack upstream tarball.
  * debian/watch:
    - Fix watch file to strip "+dfsg" suffix.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 10 Jan 2012 22:31:40 +0100

kupfer (0+v206-2) unstable; urgency=low

  * debian/patches/rhythmbox.patch:
    - rhythmbox-client has been temporarily removed upstream, kupfer
      plugin is no longer able to process playlist. Work-around this by
      invoking dbus-send utility directly.
  * debian/control:
    - Drop python-nautilus from Suggests.
  * debian/rules:
    - Pass --no-install-nautilus-extension to waf to avoid installing
      nautilus extension, it needs to be ported to PyGI in order to work
      with nautilus 3.x (Closes: #645857) (LP: #859104).

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 19 Oct 2011 22:42:38 +0200

kupfer (0+v206-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Move python-keyring to Recommends.
    - Drop python-gnome2, and python-keyring-gnome from Recommends.
    - Add python-gdata to Suggests.
    - Bump Standards-Version to 3.9.2, no changes required.
  * debian/copyright:
    - Update new copyright information.
  * debian/doc-base, debian/docs:
    - Update to new names and locations.
  * debian/rules:
    - Update to new documentation framework.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 17 Apr 2011 10:58:15 +0200

kupfer (0+v204-1) unstable; urgency=low

  * New upstream release.
    - Use INT64 signal parameter type (Closes: #615060).
  * debian/patches/debian_path.patch:
    - Removed, obsolete
  * debian/patches/evolution_contacts.patch:
    - Dropped, applied upstream.
  * debian/control:
    - Build-depend on gnome-doc-utils to allow installation of
      help files (Closes: #618499).
  * debian/rules
    - Install kupfer-activate.sh alias.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 20 Mar 2011 18:00:52 +0100

kupfer (0+v203-2) unstable; urgency=low

  * Upload to unstable.
  * Switch to dh_python2.
  * debian/copyright:
    - Update copyright years.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 08 Feb 2011 22:37:30 +0100

kupfer (0+v203-1) experimental; urgency=low

  * New upstream release (Closes: #605379).
    - Window position is no longer saved, it caused some side-effects
      such as erroneous positioning on the desktop and incorrect display
      on dual monitors (Closes: #588500, #588501).
  * debian/patches/debian_path.patch:
    - Refresh for new upstream release.
  * debian/patches/evolution_contacts.patch:
    - Do not crash if Evolution address book is missing (Closes: #606956).
  * debian/control:
    - Bump Standards-Version to 3.9.1, no changes required.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 16 Dec 2010 22:59:09 +0100

kupfer (0+v201-2) unstable; urgency=low

  * debian/control:
    - Add python-keyring-gnome to Recommends. Plugins which do require
      credentials are just a minority, so it's not strictly necessary as
      a direct dependency (Closes: #589288).

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 17 Jul 2010 14:25:38 +0200

kupfer (0+v201-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/debian_path.patch:
    - Refresh for new upstream release.
  * debian/control:
    - Build-Depend on python (>= 2.6).
    - Bump Standards-Version to 3.9.0, no changes required.
  * debian/copyright:
    - Update copyright information.
  * debian/rules:
    - Now that python2.6 is default Python version, there's no need to
      mangle shebangs to be launched by python2.6, back to default.

 -- Luca Falavigna <dktrkranz@debian.org>  Mon, 05 Jul 2010 23:48:31 +0200

kupfer (0+v200-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Tighten version of python-gtk2 to 2.16.
  * debian/watch:
    - Adjust watch file to fetch newer releases.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 11 Apr 2010 14:32:06 +0200

kupfer (0+pb1.99-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/debian_path.patch:
    - Refresh for new upstream release.
  * debian/control:
    - Build-Depend-Indep on python2.6 instead of python, this release
      is compatible with python2.6 only.
    - Depend on python-keyring.
    - Bump XS-Python-Version to >= 2.6 accordingly.
  * debian/links:
    - Install links for kupfer-exec script.
  * debian/rules:
    - Adjust shebang to work with python2.6, waiting for it to become
      the default version in Debian.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 23 Mar 2010 21:58:17 +0100

kupfer (0+pb1.1-2) unstable; urgency=low

  * Use internal copy of waf to build the package (Closes: #571707).

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 27 Feb 2010 18:41:46 +0100

kupfer (0+pb1.1-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/debian_path.patch:
    - Refresh patch for new upstream release.
  * debian/control:
    - Drop rst2man from Build-Depends.
    - Bump Standards-Version to 3.8.4, no changes needed.
  * debian/copyright:
    - Adjust copyright holders.
  * debian/watch:
    - Update to match new upstream names.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 09 Feb 2010 21:38:41 +0100

kupfer (0+c19.1-1) unstable; urgency=low

  * New upstream release.
  * Switch to format 3.0 (quilt).

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 05 Jan 2010 20:35:56 +0100

kupfer (0+c19-1) unstable; urgency=low

  [ Jakub Wilk ]
  * debian/control:
    - Replace deprecated > operator with >=.

  [ Luca Falavigna ]
  * New upstream release.
  * debian/docs: remove AUTHORS, no longer shipped in the tarball.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 25 Dec 2009 16:30:11 +0100

kupfer (0+c18.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh dependencies with minimum required versions.
  * debian/patches/debian_path.patch:
    - Upstream installs files in a private directory itself, no need to
      manually invoke Python interpreter calling kupfer script anymore.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 21 Nov 2009 19:12:36 +0100

kupfer (0+c17-2) unstable; urgency=low

  * Reintroduce kupfer-activate.sh, required for some features, also
    reintroduce debian_path.patch to adjust file locations.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 01 Nov 2009 21:30:35 +0100

kupfer (0+c17-1) unstable; urgency=low

  * New upstream release.
  * Do not use D-Bus to launch kupfer, keep it simple!
  * debian/patches/no_buildtime_depends.patch:
    - No longer needed, pass --no-runtime-deps to waf configure instead.
  * debian/patches/debian_wrappers.patch:
    - No longer needed due to packaging refactoring.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 31 Oct 2009 13:23:27 +0100

kupfer (0+c16-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release.
  * Manage new reStructuredText based documentation.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 08 Oct 2009 23:43:35 +0200

kupfer (0+c15-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/show_status_icon.patch:
    - Removed, included upstream.
  * debian/control:
    - Add python-cjson to Suggests, required by Firefox3 plugin.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 22 Sep 2009 20:45:33 +0200

kupfer (0+c14.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream release.
  * Add python-nautilus to Suggests, new nautilus plugin needs it to
    integrate itself correctly in nautilus.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 10 Sep 2009 23:04:35 +0200

kupfer (0+c10-1) unstable; urgency=low

  * New upstream release.
  * Update my e-mail address.
  * Adjust copyright informations.
  * debian/patches/show_status_icon.patch:
    - Show status icon in system tray by default.

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 27 Aug 2009 20:38:19 +0200

kupfer (0+c9.1-1) unstable; urgency=low

  * Initial release (Closes: #535617).

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Wed, 19 Aug 2009 23:32:19 +0200
